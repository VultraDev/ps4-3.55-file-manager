![Optimized-Template.png](https://bitbucket.org/repo/Axa8nM/images/3009601467-Optimized-Template.png)
# **PS4 3.55 File-browser**
Powered By [Henkaku](https://henkaku.xyz/) Exploit.

#Read file-system of PS4 in 3.15/3.50/3.55

## **Credits:**
* [CTurTe](https://twitter.com/cturte) - JuSt-ROP, the original PS4 Playground, as well as his work with 1.76.
* [Fire30](https://twitter.com/Fire30_) - The porting of the WebKit Exploit to PS4.
* [TheoryWrong](https://twitter.com/TheoryWrong)
* Anonymous contributor
* And all other people contribute for this project

# **Vultr Changed Log :**
* Test Sockets Now Working
* Encrypted Files

# **Upcoming Features:**
* N/A

# **Sources:**
* Original Sourced by @[TheoryWrong](https://twitter.com/TheoryWrong).
* Modified Version By Vultra ([Twitter](https://twitter.com/VultraDev/)).